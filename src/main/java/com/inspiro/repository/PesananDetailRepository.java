package com.inspiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inspiro.entity.PesananDetail;

public interface PesananDetailRepository extends JpaRepository<PesananDetail, Long>{

}
