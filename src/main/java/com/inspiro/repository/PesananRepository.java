package com.inspiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inspiro.entity.Pesanan;

public interface PesananRepository extends JpaRepository<Pesanan, Long>{

}
