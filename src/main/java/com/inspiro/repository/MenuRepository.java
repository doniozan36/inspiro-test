package com.inspiro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inspiro.entity.Menu;


public interface MenuRepository extends JpaRepository<Menu, Long>{
//	@Query(value = "Select c from Menu c where c.namaMenu like %:namaMenu%")
//	List<Menu> getMenuBySearchNamaMenu(@Param("namaMenu") String namaMenu);
	
	List<Menu> findByNamaMenuIgnoreCaseContaining(String namaMenu);
}
