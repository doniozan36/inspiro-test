package com.inspiro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inspiro.entity.Users;

public interface UserRepository extends JpaRepository<Users, Long>{
	Users findByUsername(String username);
}