package com.inspiro.service;

import java.util.List;
import com.inspiro.entity.Menu;

public interface MenuService {

	List<Menu> getMenu(String namaMenu);

	List<Menu> saveMenu(Menu menu);

	List<Menu> deleteMenu(long id);

}
