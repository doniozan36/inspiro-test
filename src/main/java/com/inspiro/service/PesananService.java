package com.inspiro.service;

import java.util.List;

import com.inspiro.entity.Pesanan;

public interface PesananService {

	Long savePesanan(Pesanan pesanan);

	List<Pesanan> savePesananDetail(List<Long> idMenu, List<Long> qty, Long pesananId);

}
