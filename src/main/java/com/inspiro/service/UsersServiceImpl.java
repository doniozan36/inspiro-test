package com.inspiro.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inspiro.repository.UserRepository;
import com.inspiro.entity.Users;

@Service
@Transactional
public class UsersServiceImpl implements UsersService{

	@Autowired
	UserRepository usersRepo;
	
	@Override
	public Users getByUsername(String username) {
		Users user = usersRepo.findByUsername(username);
		return user;
	}
}
