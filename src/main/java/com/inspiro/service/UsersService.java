package com.inspiro.service;

import java.util.List;

import com.inspiro.entity.Pesanan;
import com.inspiro.entity.Users;

public interface UsersService {

	Users getByUsername(String username);

}
