package com.inspiro.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.inspiro.entity.Menu;
import com.inspiro.repository.MenuRepository;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

	@Autowired
	MenuRepository menuRepo;
	
	@Override
	public List<Menu> getMenu(String namaMenu){
		List<Menu> list = new ArrayList<Menu>();
		menuRepo.findByNamaMenuIgnoreCaseContaining(namaMenu).forEach(list::add);
	    return list;
	}
	
	@Override
	public List<Menu> saveMenu(Menu menu){
		menuRepo.save(menu);
		List<Menu> list = new ArrayList<Menu>();
		menuRepo.findAll().forEach(list::add);
	    return list;
	}
	
	@Override
	public List<Menu> deleteMenu(long id){
		menuRepo.deleteById(id);
		List<Menu> list = new ArrayList<Menu>();
		menuRepo.findAll().forEach(list::add);
	    return list;
	}
}
