package com.inspiro.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inspiro.repository.PesananRepository;
import com.inspiro.entity.Menu;
import com.inspiro.entity.Pesanan;
import com.inspiro.entity.PesananDetail;
import com.inspiro.repository.MenuRepository;
import com.inspiro.repository.PesananDetailRepository;

@Service
@Transactional
public class PesananServiceImpl implements PesananService{

	@Autowired
	PesananRepository pesananRepo;
	
	@Autowired
	PesananDetailRepository pesananDetailRepo;
	
	@Autowired
	MenuRepository menuRepo;
	
	@Override
	public Long savePesanan(Pesanan pesanan) {
		pesanan = pesananRepo.save(pesanan);
		pesanan.setNoTrans(invoiceNo(pesanan.getId()));
		pesanan = pesananRepo.save(pesanan);
		return pesanan.getId();
	}
	
	@Override
	public List<Pesanan> savePesananDetail(List<Long> idMenu, List<Long> qty, Long pesananId) {
		for (int i = 0; i < idMenu.size(); i++) {
			Optional<Menu> menu = menuRepo.findById(idMenu.get(i));
			PesananDetail pesananDetail = new PesananDetail();
			pesananDetail.setHarga(menu.get().getHarga());
			pesananDetail.setJumlah(qty.get(i));
			pesananDetail.setNamaMenu(menu.get().getNamaMenu());
			pesananDetail.setPesananId(pesananId);
			pesananDetail = pesananDetailRepo.save(pesananDetail);
		}
		List<Pesanan> list = new ArrayList<Pesanan>();
		pesananRepo.findAll().forEach(list::add);
		return list;
	}
	
	public static String invoiceNo(Long referenceNo) {		
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String yearMonthdd = df.format(Calendar.getInstance().getTime());
        
        String invoiceNo = String.format("INV/%s/INSPIRO/%09d", yearMonthdd, referenceNo);
        
		return invoiceNo;
	}
}
