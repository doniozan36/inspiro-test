package com.inspiro;

import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import com.github.benmanes.caffeine.cache.Caffeine;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableScheduling
public class MainServiceApplication {
	
	private static final Logger LOGGER = LogManager.getLogger(MainServiceApplication.class);
	
	public void init(){
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	public static void main(String[] args) {
		SpringApplication.run(MainServiceApplication.class, args);
		LOGGER.info("Service RUNNING");
	}
	
	@Bean
	public Caffeine<?, ?> caffeineConfig() {
	    return Caffeine.newBuilder().expireAfterWrite(60, TimeUnit.MINUTES);
	}

}
