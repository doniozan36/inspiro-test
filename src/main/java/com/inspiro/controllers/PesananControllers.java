package com.inspiro.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inspiro.entity.Pesanan;
import com.inspiro.service.PesananService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/pesanan")
@Validated
@CrossOrigin
@RequiredArgsConstructor
public class PesananControllers {
	
	@Autowired
    private PesananService pesananService;
	
	@PostMapping("/keranjang")
	public ResponseEntity<?> keranjang(
		@RequestParam(name="totalBayar") Long totalBayar,
		@RequestParam(name="pembayaran") String pembayaran,
		@RequestParam(name="idMenu") List<Long> idMenu,
		@RequestParam(name="qty") List<Long> qty
		)
    {
		Pesanan pesanan = new Pesanan();
		pesanan.setPembayaran(pembayaran);
		pesanan.setTotalBayar(totalBayar);
		Long pesananId = pesananService.savePesanan(pesanan);
		return ResponseEntity.status(HttpStatus.OK).body(pesananService.savePesananDetail(idMenu, qty, pesananId));
    }
}
