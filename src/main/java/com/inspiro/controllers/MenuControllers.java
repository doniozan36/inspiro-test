package com.inspiro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inspiro.entity.Menu;
import com.inspiro.service.MenuService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/menu")
@CrossOrigin
public class MenuControllers {
	
	@Autowired
    private MenuService menuService;
	
	@PostMapping("/list")
	public ResponseEntity<?> getMenu(
		@RequestParam(name="namaMenu", required=false) String namaMenu)
    {
		return ResponseEntity.status(HttpStatus.OK).body(menuService.getMenu(namaMenu));
    }
	
	@PostMapping("/save")
	public ResponseEntity<?> saveMenu(
		@RequestParam(name="namaMenu", required=false) String namaMenu,
		@RequestParam(name="harga") Long harga
	){
		Menu menu = new Menu();
		menu.setHarga(harga);
		menu.setNamaMenu(namaMenu);
		return ResponseEntity.status(HttpStatus.OK).body(menuService.saveMenu(menu));
	}
	
	@PostMapping("/update")
	public ResponseEntity<?> updateMenu(
		@RequestParam(name="namaMenu", required=false) String namaMenu,
		@RequestParam(name="harga") Long harga,
		@RequestParam(name="id") Long id
	){
		Menu menu = new Menu();
		menu.setHarga(harga);
		menu.setNamaMenu(namaMenu);
		menu.setId(id);
		return ResponseEntity.status(HttpStatus.OK).body(menuService.saveMenu(menu));
	}
	
	@PostMapping("/delete")
	public ResponseEntity<?> deleteMenu(
		@RequestParam(name="id") Long id)
    {
		return ResponseEntity.status(HttpStatus.OK).body(menuService.deleteMenu(id));
    }
}
