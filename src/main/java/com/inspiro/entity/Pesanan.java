package com.inspiro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Data
@Entity(name = "pesanan")
@EntityListeners(AuditingEntityListener.class)
public class Pesanan {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	
    @Column(name="no_trans")
    private String noTrans;
    
    @Column(name="total_bayar")
    private Long totalBayar;
    
    @Column(name="pembayaran")
    private String pembayaran;
}
