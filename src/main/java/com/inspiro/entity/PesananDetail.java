package com.inspiro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Data
@Entity(name = "pesanan_detail")
@EntityListeners(AuditingEntityListener.class)
public class PesananDetail {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	
    @Column(name="pesanan_id")
    private Long pesananId;
    
    @Column(name="nama_menu")
    private String namaMenu;
    
    @Column(name="harga")
    private Long harga;
    
    @Column(name="jumlah")
    private Long jumlah;
}
